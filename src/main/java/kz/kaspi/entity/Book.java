package kz.kaspi.entity;

import javax.persistence.*;

@Entity
@Table(name = "books")
public class Book {
    public Book(){}

    public Book(String name, String author, String publication){
        this.name = name;
        this.author = author;
        this.publication = publication;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column
    private String author;

    @Column
    private String publication;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPublication() {
        return publication;
    }

    public void setPublication(String publication) {
        this.publication = publication;
    }
}
