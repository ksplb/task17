<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Home library</title>
</head>
<body>
    <h1> Welcome to the Home Library! </h1>
    <br/>
    <a href="./books/add.jsp">Add new book</a>
    <br>
    <a href="./getBooksServlet">Browse all books</a>
</body>
</html>