package kz.kaspi.servlet;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import kz.kaspi.entity.Book;
import kz.kaspi.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.io.IOException;

@WebServlet(name = "addBookServlet", value = "/addBookServlet")
public class addBookServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String author = request.getParameter("author");
        String publication = request.getParameter("publication");

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Book newBook = new Book(name, author, publication);
        session.save(newBook);
        transaction.commit();
        session.close();

        response.sendRedirect("getBooksServlet");
    }
}
