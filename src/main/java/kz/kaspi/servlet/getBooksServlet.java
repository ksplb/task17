package kz.kaspi.servlet;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import kz.kaspi.entity.Book;
import kz.kaspi.util.HibernateUtil;
import org.hibernate.Session;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "getBooksServlet", value = "/getBooksServlet")
public class getBooksServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Book> books = session.createQuery("from Book", Book.class).getResultList();
        session.close();
        request.setAttribute("books", books);
        getServletContext().getRequestDispatcher("/books/get.jsp").forward(request, response);
    }
}
