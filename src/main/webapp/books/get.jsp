<%--
  Created by IntelliJ IDEA.
  User: Yerniyaz
  Date: 15.04.2021
  Time: 19:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Book list</title>
    </head>
    <body>
        <table>
            <th>
                <td>Название</td>
                <td>Автор</td>
                <td>Дата публикации и издание</td>
            </th>
            <c:forEach items="${books}" var="book">
                <tr>
                    <td>${book.name}</td>
                    <td>${book.author}</td>
                    <td>${book.publication}</td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
